<?php
/* Script is checking free memory left on server disk.    */
/* Then inserting information into databse.               */
/* databaseConnection.class.php needed!                   */
/* Author: Michal Galeziowski                             */

 	require_once('databaseConnection.class.php');

 	$conn = new databaseConnection;

 	$bytes = disk_free_space("."); 
    $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
    $base = 1024;
    $class = min((int)log($bytes , $base) , count($si_prefix) - 1);
    //echo $bytes . '<br />';
    $output = sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class];

//    echo $output;
    $query = "INSERT INTO michalg.disk_size_check(free_space) VALUE ('".$output."');";

    try{
    	 $conn->insertQuery($query);
    		 
    }catch(PDOException $e){
    	echo $e;
    }

   


?>