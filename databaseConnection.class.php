<?php
//database connection class
// needs to have config.ini in directory
class databaseConnection{
	private $db_host;
	private $db_user;
	private $db_pass;
	private $port;

	public $conn;


	private function connect(){
		
		try{
			$this->conn = new PDO('mysql:host='.$this->db_host.'; dbname=;port='.$this->port, $this->db_user, $this->db_pass);
			//file_put_contents('dblog.txt','['.date("d/M/Y H:i:s").'] '.  basename($_SERVER["SCRIPT_FILENAME"]) .' Connected'.PHP_EOL, FILE_APPEND);

		}catch(PDOexception $e){
			file_put_contents('dblog.txt','['.date("d/M/Y H:i:s").'] '.  basename($_SERVER["SCRIPT_FILENAME"]) .' Cannot connect: ' . $e->getMessage().PHP_EOL, FILE_APPEND);;
		}
	}

	public function runQuery($query){
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$res  = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $res;
		
	}

	public function insertQuery($query){
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

	}


	public function __construct(){
		$login_info = parse_ini_file('config.ini');
		$this->db_host = $login_info['host'];
		$this->port = $login_info['port'];
		$this->db_pass = $login_info['pass'];
		$this->db_user = $login_info['user'];
		$this->connect();
		return $this->conn;
	}


}

?>